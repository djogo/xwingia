#!/usr/bin/php
<?php
$dpi = 300;
//$width_in_cm = 23.7; // A3
//$height_in_cm = 36.0;
//$width_in_cm = 96; // board 96x96 + 5cm margem + sangria
//$height_in_cm = 96;
list($width_in_cm, $height_in_cm) = array( 90, 90 ); // thamonik
$margin_in_cm = 0;

$big_step_cm = 1.0;
$small_step_cm = 0.5;

$mark_in_cm = 0.2;

$dpcm = $dpi / 2.54;
$strokeWidth = 0.01*$dpcm;


$width_in_px = floor((2*$margin_in_cm + $width_in_cm) * $dpcm);
$height_in_px = floor((2*$margin_in_cm + $height_in_cm) * $dpcm);

#echo "$width_in_px = (2*$margin_in_cm + $width_in_cm) * $dpcm\n";
#echo "$height_in_px = (2*$margin_in_cm + $height_in_cm) * $dpcm\n";
#printf("%d x %d\n",$width_in_px,$height_in_px);

$draw      = new \ImagickDraw(); //$width_in_px,$height_in_px);
$draw->setStrokeWidth($strokeWidth);

$orange= 'orange'; # imagecolorallocate($im, 220, 210, 60);
$white = 'white';  # imagecolorallocate($draw, 0xff, 0xff, 0xff);
$black = 'black';  # imagecolorallocate($draw, 0x00, 0x00, 0x00);
$gray  = 'rgb(200,200,200)';   # imagecolorallocate($draw, 0xa0, 0xa0, 0xa0);

//myimagefilledrectangle($draw, 0, 0, $width_in_px, $height_in_px, $white);

// margins + bleed
if ( $margin_in_cm > 0 ) {
    echo "margens...\n";
	myimagefilledrectangle($draw, 0, 0, $margin_in_cm * $dpcm, $margin_in_cm * $dpcm, $orange );
	myimagefilledrectangle($draw, $width_in_px, 0, $width_in_px-$margin_in_cm * $dpcm, $margin_in_cm * $dpcm, $orange );
	myimagefilledrectangle($draw, $width_in_px, $height_in_px, $width_in_px - $margin_in_cm * $dpcm, $height_in_px - $margin_in_cm * $dpcm, $orange );
	myimagefilledrectangle($draw, 0, $height_in_px, $margin_in_cm * $dpcm, $height_in_px - $margin_in_cm * $dpcm, $orange );
}

echo "desenhando grid vertical...\n";
$x = 0; // cm
while ( $x < $width_in_cm ) {
    myimageline( $draw, ($margin_in_cm + $x) * $dpcm, 0, ($margin_in_cm + $x) * $dpcm, $height_in_px, $black );
    $y = 0; // cm
    while ( $y < $height_in_cm ) {
	if ( ($y-floor($y)) > 0.01 && ($y-floor($y))<0.99 ) {
	    myimageline( $draw
    		, ($margin_in_cm + $x) * $dpcm
    		, ($margin_in_cm + $y) * $dpcm
    		, ($margin_in_cm + $x + $mark_in_cm) * $dpcm
    		, ($margin_in_cm + $y ) * $dpcm
    		, $black );
	}
        $y += $small_step_cm;
    }


    $x += $big_step_cm;
}

echo "desenhando grid horizontal...\n";
$y = 0; // cm
while ( $y < $height_in_cm ) {
    myimageline( $draw, 0, ($margin_in_cm + $y) * $dpcm, $width_in_px, ($margin_in_cm + $y) * $dpcm, $black );
    $x = 0.0; // cm
    while ( $x < $width_in_cm ) {
	if ( ($x-floor($x)) > 0.01 && ($x-floor($x))<0.99 ) {
	    myimageline( $draw, ($margin_in_cm + $x) * $dpcm, ($margin_in_cm + $y) * $dpcm, ($margin_in_cm + $x) * $dpcm, ($margin_in_cm + $y + $mark_in_cm) * $dpcm, $black );
	}
        $x += $small_step_cm;
    }

    $y += $big_step_cm;
}

// cria a imagem para poder escrever por cima
$imagick = new Imagick();
$imagick->newImage( $width_in_px, $height_in_px, $white );
$imagick->drawImage($draw);


// escreve titulos
echo "escrevendo coordenadas...\n";
$x = 0; // cm
while ( $x < $width_in_cm ) {
    $y = 0; // cm
    while ( $y < $height_in_cm ) {
	myimagestring($imagick, $draw, 3
		, ($margin_in_cm + $x + 0) * $dpcm
		, ($margin_in_cm + $y + 0) * $dpcm + 20 
		, sprintf("%d,%d",$x,$height_in_cm - $y - 1)
		, $black
		, 20 );
        $y += $big_step_cm;
    }
    $x += $big_step_cm;
}


echo "saving png...\n";
myimagepng($imagick,'teste.png');
echo "saving svg...\n";
myimagesvg($imagick,'teste.svg');
echo "finishing...\n";
myimagedestroy($draw);

function myimagedestroy($im) {
    // não implementada
}

function myimagepng($imagick,$filename) {
    $imagick->setImageFormat('png');
    file_put_contents( $filename, $imagick );
}

function myimagesvg($imagick,$filename) {
    $imagick->setImageFormat('svg');
    file_put_contents( $filename, $imagick );
}


function myimagefilledrectangle($im,$x0,$y0,$x1,$y1,$clr){
    $im->setFillColor($clr);
    $im->rectangle($x0, $y0, $x1, $y1);
}

function myimageline( $im, $x0, $y0, $x1, $y1, $clr ) {
    $im->setStrokeColor($clr);
    $im->line($x0,$y0,$x1,$y1);
}

function myimagestring( $imagick, $draw, $font, $x, $y, $text, $clr, $fontsize ) {
    $draw->setFillColor($clr);
    $draw->setFontSize( $fontsize );
    #$draw->setTextAlignment(Imagick::ALIGN_CENTER);
    #$draw->setGravity(Imagick::GRAVITY_NORTH);
    $imagick->annotateImage( $draw, $x, $y, 0, $text );
}
