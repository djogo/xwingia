#!/usr/bin/python3
#Encoding: utf8
#
# sizes: https://boardgamegeek.com/thread/1337514/dimensions-radii-and-arc-lenghts-maneuver-template
#
# range in mm: 100.0, 200.0, 300.0
#
# size in mm: 40.0, 80.0, 120.0, 160.0, 200.0
#
# Turn inside radii in mm: 25.0, 53.0, 80.0
# Turn outside radii in mm: 45.0, 73.0, 100.0
#
# Bank inside radii in mm: 70.0, 120.0, 170.0
# Bank outside radii in mm: 90.0, 140.0, 190.0 
#
#
#
import copy
from vector import *
from history import  *
from maneuver import *
from attack import *
from dogFight import *
from dial import *
from action import *
from ship import *
from history import *
from board import *
from obstacle import *
from consoleDraw import *

'''
    (Ship, Vector) => [ Maneuver ]
'''
def ParkShip( ship, destination, desiredAngle=None ):
    if ship.isOutOfBoard():
        return False

    allClones = []
    allClones.append(ship.copy())

    j = 0
    MAXPATHLENGTH = 7
    MAXCLONES = 9999
    closest = allClones[0]
    while len(allClones)>0 and j<MAXCLONES:
        myship = allClones.pop(0) 

        possibleManeuvers = myship.possibleManeuvers()
        for i in possibleManeuvers:
            if myship.history.len() > MAXPATHLENGTH:
                continue
            shipClone = myship.copy()
            shipClone.setDialTo( i )
            shipClone.move()

            if shipClone.isOutOfBoard():
                continue
            if shipClone.beenThereBefore():
                continue

            if shipClone.dist( destination ) < closest.dist( destination ):
                if desiredAngle == None or desiredAngle == shipClone.angle:
                    if shipClone.dist(destination) < EPSILON:
                        return shipClone
                    closest = shipClone

            allClones.append( shipClone )

            for k in shipClone.possibleActions():
                if myship.history.len() > MAXPATHLENGTH:
                    continue
                shipClone2 = shipClone.copy()
                shipClone2.doAction(k)

                if shipClone2.isOutOfBoard():
                    continue
                if shipClone2.beenThereBefore():
                    continue

                if shipClone2.dist( destination ) < closest.dist( destination ):
                    if desiredAngle == None or desiredAngle == shipClone2.angle:
                        if shipClone2.dist(destination) < EPSILON:
                            return shipClone2
                        closest = shipClone2
                

                allClones.append( shipClone2 )

        j=j+1

    return closest


def testPark():
    print(("PARKING TEST"))
    x = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( x )
    x.setPosition( 45, 45 )

    pos = Vector( 65.0, 65.0 )
    print( ("\nGoing to %s pointing anywhere" % pos))
    print( (ParkShip( x,pos ).history))

    pos = Vector( 55.0, 55.0 )
    print(("\nGoing to %s pointing left" % pos))
    print((ParkShip( x,pos,90.0 ).history))

    pos = Vector( 80.0, 20.0 )
    print(("\nGoing to %s pointing down" % pos))
    print((ParkShip( x,pos,180.0 ).history))

    pos = Vector( 10.0, 10.0 )
    print( ("\nGoing to %s pointing left" % pos))
    print( ParkShip( x,pos,90.0 ).history)

def main():
    print("teste inicial!")
    #class TieAdvanced(Ship):
    myxwing = LukeSkywalkerXwing()
    print( myxwing)

    # nao tem q funcionar
    print("esperado: falha")
    myxwing.setDialTo( ManeuverHardRight1() )

    # fazer sem liberar stress
    print("esperado: ok")
    myxwing.setDialTo( ManeuverHardRight3() )
    print( myxwing.dial)
    myxwing.move()
    print( myxwing)

    # tem q funcionar
    print("esperado: koiogran!")
    myxwing.setDialTo( ManeuverKoiogran4() )
    print( myxwing.dial)
    myxwing.move()
    print( myxwing)

    # não pode fazer pq stress
    myxwing.setDialTo( ManeuverKoiogran4() )
    print( myxwing.dial)
    myxwing.move()
    print( myxwing)

    # fazer sem liberar stress
    myxwing.setDialTo( ManeuverHardRight3() )
    print( myxwing.dial)
    myxwing.move()
    print( myxwing)

    # fazer e remover stress
    myxwing.setDialTo( ManeuverSoftLeft1() )
    print( myxwing.dial)
    myxwing.move()
    print( myxwing.history)

def test2():
    myxwing = LukeSkywalkerXwing()
    myxwing.setDialTo( ManeuverSoftLeft1() )
    myxwing.move()
    print( myxwing)
    myxwing.setDialTo( ManeuverSoftRight1() )
    myxwing.move()
    print( myxwing)
    myxwing.setDialTo( ManeuverSoftRight1() )
    myxwing.move()
    print( myxwing)
    myxwing.setDialTo( ManeuverSoftLeft1() )
    myxwing.move()
    print( myxwing)
    if abs( myxwing.positionX ) < EPSILON:
        print( "SUCESSO")
        return True
    else:
        print( "deveria estar centralizado??")
        return False

def test2b():
    print( "TESTE 2b")
    myxwing = LukeSkywalkerXwing()
    myxwing.setDialTo( ManeuverHardRight2() )
    myxwing.move()
    print( myxwing)
    myxwing.setDialTo( ManeuverHardLeft2() )
    myxwing.move()
    print( myxwing)
    myxwing.setDialTo( ManeuverHardLeft2() )
    myxwing.move()
    print( myxwing)
    myxwing.setDialTo( ManeuverHardRight2() )
    myxwing.move()
    print( myxwing)
    if abs( myxwing.positionX ) < EPSILON:
        print( "SUCESSO")
        return True
    else:
        print( "deveria estar centralizado??")
        return False

def test2c():
    print( "TESTE 2C")
    myxwing = LukeSkywalkerXwing()
    myxwing.setDialTo( ManeuverHardLeft2() )
    myxwing.move()
    print( myxwing)
    myxwing.setDialTo( ManeuverHardRight2() )
    myxwing.move()
    print( myxwing)
    if myxwing.dist( Vector(-16.6,16.6) ) < EPSILON:
        print( "SUCESSO")
        return True
    else:
        print( "Não acaba perto de -16.6,16.6")
        return False

def test3():
    print( "TESTE 3")
    myxwing = LukeSkywalkerXwing()
    for i in range(4):
        myxwing.setDialTo( ManeuverHardLeft3() )
        myxwing.move()
        print( myxwing)
    for i in range(4):
        myxwing.setDialTo( ManeuverHardRight3() )
        myxwing.move()
        print( myxwing)
    if myxwing.dist( Vector(0,0) ) < EPSILON:
        print( "SUCESSO")
        return True
    else:
        print( "Não acaba perto de 0,0")
        return False

def test4():
    print( "\nTeste 4 - SoontirFelTieInterceptor")
    mytie = SoontirFelTieInterceptor()
    board = Board(90,90)
    board.addShip( mytie )
    mytie.setPosition( 45, 45 )

    pos = Vector( 65.0, 65.0 )
    print( "\nGoing to %s pointing anywhere" % pos)
    print( ParkShip( mytie,pos ).history)

def test4b():
    print( "\nTeste 4b - SoontirFelTieInterceptor boost")
    mytie = SoontirFelTieInterceptor()
    mytie.setDialTo( ManeuverStraight4() )
    mytie.move()
    mytie.doAction( SoftRightBoostAction() )
    print( mytie.history)

def testParkInterceptor():
    print( "\nPARKING TEST - interceptor")
    x = SoontirFelTieInterceptor()
    board = Board(90,90)
    board.addShip( x )
    x.setPosition( 45, 45 )

    pos = Vector( 65.0, 65.0 )
    print( "\nGoing to %s pointing anywhere" % pos)
    print( ParkShip( x,pos ).history)

    pos = Vector( 16.0, 80.0 )
    print( "\nGoing to %s pointing anywhere" % pos)
    print( ParkShip( x,pos ).history)

    pos = Vector( 39.0, 61.0 )
    print( "\nGoing to %s pointing anywhere" % pos)
    print( ParkShip( x,pos, 0 ).history)


def testBarrel():
    print( "\nBarrel Roll TEST - interceptor")
    x = SoontirFelTieInterceptor()
    board = Board(90,90)
    board.addShip( x )
    x.setPosition( 45, 45 )
    x.evaluateActions()
    
    barrel = BarrelRollAction()
    barrel.setDirectionLeft()
    barrel.setForwardDisplacement(0)

    print(x)
    x.doAction( barrel )
    print(x)
    # expected: 37,45
    barrel.setDirectionRight()
    barrel.setForwardDisplacement(0)
    x.doAction( barrel )
    print(x)
    # expected: 45, 45
    barrel.setDirectionLeft()
    barrel.setForwardDisplacement(1)
    x.doAction( barrel )
    print(x)
    # expected: 37,47
    barrel.setDirectionRight()
    barrel.setForwardDisplacement(-1)
    x.doAction( barrel )
    print(x)
    # expected: 45,45
    x.angle = 45
    barrel.setDirectionLeft()
    barrel.setForwardDisplacement(1)
    x.doAction( barrel )
    print(x)
    # expected: ??,??

def testGame():
    print( "\nTest Game!")
    sf = SoontirFelTieInterceptor()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )
    board.addShip( ls )
    sf.setPosition( 17,35,180 )
    ls.setPosition( 13,0,0 )
    
    # round start
    sf.setDialTo( ManeuverStraight2() )
    ls.setDialTo( ManeuverStraight1() )

    # activation phase
    # TODO: board do that, ordered by pilot skill
    ls.move()
    ls.doAction( FocusAction() )

    sf.move()
    sf.doAction( FocusAction() )    
    print( len(sf.possibleActions()) == 0 )

    # TODO combat phase
    #
    #
    sfAttack = sf.attack( ls )
    print( sfAttack)
    lsAttack = ls.attack( sf )
    print( lsAttack)

    # end phase
    # TODO board do that
    ls.resetTokens()
    sf.resetTokens()

    print("End of round 1")
    print(sf)
    print(ls)

    # second round - sf will attack ls from behind
    sf.setDialTo( ManeuverKoiogran5() )
    ls.setDialTo( ManeuverStraight1() )

    # activation phase
    # TODO: board do that, ordered by pilot skill
    ls.move()
    ls.doAction( FocusAction() )

    sf.move()
    print( len(sf.possibleActions()) == 0 ) # no actions. (stressed)

    # TODO combat phase
    #
    #
    sfAttack = sf.attack( ls )
    print( sf.angleToShip( ls ) )
    print( sfAttack )
    lsAttack = ls.attack( sf )
    print( lsAttack)

    # end phase
    # TODO board do that
    ls.resetTokens()
    sf.resetTokens()

    print("End of round 2")
    print(sf) #depois do koiogran não deveria ter X=13? tá dando 14.6
    print(ls)


def testGame2():
    print( "\nTest Game 2!")
    sf = SoontirFelTieInterceptor()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )
    board.addShip( ls )
    sf.setPosition( 47.8284,42.1715,225 ) #pointing south-west
    ls.setPosition( 0,0,45 ) # pointing north-east
    
    # round start
    sf.setDialTo( ManeuverStraight2() )
    ls.setDialTo( ManeuverStraight3() )

    # activation phase
    # TODO: board do that, ordered by pilot skill
    ls.move()
    ls.doAction( FocusAction() )

    sf.move()
    sf.doAction( FocusAction() )    

    # TODO combat phase
    #
    #
    sfAttack = sf.attack( ls )

    print( sfAttack) # out of range
    lsAttack = ls.attack( sf )
    print( lsAttack) #out of range

    # end phase
    # TODO board do that
    ls.resetTokens()
    sf.resetTokens()

    print("End of round 1")
    print(sf)
    print(ls)
    
    # round 2 start
    sf.setDialTo( ManeuverStraight5() )
    ls.setDialTo( ManeuverKoiogran4() )

    # activation phase
    # TODO: board do that, ordered by pilot skill
    ls.move()
    ls.doAction( FocusAction() )

    sf.move()
    sf.doAction( FocusAction() )    

    # TODO combat phase
    #
    #
    sfAttack = sf.attack( ls )

    print( sfAttack) # out of range
    lsAttack = ls.attack( sf )
    print( lsAttack) #out of range

    # end phase
    # TODO board do that
    ls.resetTokens()
    sf.resetTokens()

    print("End of round 2")
    print(sf)
    print(ls)
    print(ls.angleToShip(sf))
    
def testSquare():
    print( "\nTest Square!")
    sf = SoontirFelTieInterceptor()
    sf.setPosition(0,0,0)
    s = sf.getSquare()
    r1 = (s[0] == Vector(0,0)) and (s[1] == Vector(4.0,0.0)) and (s[2] == Vector(4,-4)) and (s[3] == Vector(0,-4))
    if not r1:
        print("Error evaluating r1")
        print(s[0])
        print(s[1])
        print(s[2])
        print(s[3])
    sf.angle=90
    s = sf.getSquare()
    r2 = (s[0] == Vector(0,0)) and (s[1] == Vector(0,-4)) and (s[2] == Vector(-4,-4)) and (s[3] == Vector(-4,0.0))
    if not r2:
        print("Error evaluating r2")
        print(s[0])
        print(s[1])
        print(s[2])
        print(s[3])
    print(r1 and r2)

def testCollision():
    print( "\nTest Collision - straight maneuver!")
    sf = SoontirFelTieInterceptor()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )
    board.addShip( ls )
    sf.setPosition(   4, 16,180 ) #pointing south
    ls.setPosition(   0,  0,  0 ) # pointing north
    
    # round start
    ls.setDialTo( ManeuverStraight4() )

    # activation phase
    # TODO: board do that, ordered by pilot skill
    draw_board( board )
    input("none")
    ls.move()
    draw_board( board )


    input("none")
    print(ls.collisionThisTurn)

    print(ls)

def testCollision2():
    print( "\nTest Collision 1 bank!")
    sf = SoontirFelTieInterceptor()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )
    board.addShip( ls )
    sf.setPosition(   5,  8, 0 ) #pointing north
    ls.setPosition(   1,  1,  0 ) # pointing north
    
    # round start
    ls.setDialTo( ManeuverSoftRight1() )

    # activation phase
    # TODO: board do that, ordered by pilot skill
    draw_board( board )
    input("none")
    ls.move()
    print(ls.collisionThisTurn)
    draw_board( board )
    input("none")

    print(ls)


def testCollision3():
    print( "\nTest Collision - koiogran!")
    sf = SoontirFelTieInterceptor()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )
    board.addShip( ls )
    sf.setPosition(   4, 16,180 ) #pointing south
    ls.setPosition(   0,  0,  0 ) # pointing north
    
    # round start
    ls.setDialTo( ManeuverKoiogran4() )

    # activation phase
    # TODO: board do that, ordered by pilot skill
    draw_board( board )
    input("none")
    ls.move()
    draw_board( board )
    input("none")
    print(ls.collisionThisTurn)

def testCollision4():
    print( "\nTest Collision - sem colisão!")
    sf = SoontirFelTieInterceptor()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )
    board.addShip( ls )
    sf.setPosition(   4, 16,180 ) #pointing south
    ls.setPosition(   0,  0,  0 ) # pointing north
    
    # round start
    ls.setDialTo( ManeuverStraight1() )

    # activation phase
    # TODO: board do that, ordered by pilot skill
    draw_board( board )
    input("none")
    ls.move()
    draw_board( board )
    input("none")
    print(ls.collisionThisTurn)


def testPartialBank1(angle):
    print("Testing 1 bank at angle=%d"%angle)
    sf = LukeSkywalkerXwing()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )

    sf.setPosition(   0,  0,  angle ) # pointing north
    sf.setDialTo( ManeuverSoftLeft1() )
    sf.move()  

    board.addShip( ls )
    ls.setPosition(   0,  0, angle ) # pointing north

    scale = 4
    draw_board( board )    
    completeness = 0.0
    #ls.setDialTo( ManeuverSoftRight1() )
    ls.setDialTo( ManeuverSoftLeft1() )
    selectedManeuver = ls.dial.selectedManeuver()
    while completeness < 1.0 :

        ls.setPosition(   0,  0,  angle ) # pointing north

        # code from ship.move
        dv, angleIncrease = selectedManeuver.maneuver.getDisplacementVector( ls.baseSizeInCm, ls.baseDistanceToTemplate, ls.angle, completeness )

        # evaluate final position
        ls.positionX += dv.x 
        ls.positionY += dv.y 
        ls.angle = ( ls.angle + angleIncrease + 360 ) % 360
        plot_polygon( ls.getShapelyPolygon(), scale )


        completeness += 0.1



    # not sure what to test
    input("none")

def testPartialBank2():
    sf = LukeSkywalkerXwing()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )

    sf.setPosition(   0,  0,  0 ) # pointing north
    sf.setDialTo( ManeuverSoftRight2() )
    sf.move()  

    board.addShip( ls )
    ls.setPosition(   0,  0,  0 ) # pointing north

    scale = 4
    draw_board( board )    
    completeness = 0.0
    ls.setDialTo( ManeuverSoftRight2() )
    selectedManeuver = ls.dial.selectedManeuver()
    while completeness < 1.0 :

        ls.setPosition(   0,  0,  0 ) # pointing north

        # code from ship.move
        dv, angleIncrease = selectedManeuver.maneuver.getDisplacementVector( ls.baseSizeInCm, ls.baseDistanceToTemplate, ls.angle, completeness )

        # evaluate final position
        ls.positionX += dv.x 
        ls.positionY += dv.y 
        ls.angle = ( ls.angle + angleIncrease + 360 ) % 360
        plot_polygon( ls.getShapelyPolygon(), scale )


        completeness += 0.1



    # not sure what to test
    input("none")

def testPartialBank3():
    sf = LukeSkywalkerXwing()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )

    sf.setPosition(   0,  0,  0 ) # pointing north
    sf.setDialTo( ManeuverSoftRight3() )
    sf.move()  

    board.addShip( ls )
    ls.setPosition(   0,  0,  0 ) # pointing north

    scale = 4
    draw_board( board )    
    completeness = 0.0
    ls.setDialTo( ManeuverSoftLeft3() )
    selectedManeuver = ls.dial.selectedManeuver()
    while completeness < 1.0 :

        ls.setPosition(   0,  0,  0 ) # pointing north

        # code from ship.move
        dv, angleIncrease = selectedManeuver.maneuver.getDisplacementVector( ls.baseSizeInCm, ls.baseDistanceToTemplate, ls.angle, completeness )

        # evaluate final position
        ls.positionX += dv.x 
        ls.positionY += dv.y 
        ls.angle = ( ls.angle + angleIncrease + 360 ) % 360
        plot_polygon( ls.getShapelyPolygon(), scale )


        completeness += 0.1



    # not sure what to test
    input("none")


def testObstacle():
    print("-- teste de obstaculo")
    sf = LukeSkywalkerXwing()
    ls = LukeSkywalkerXwing()
    board = Board(90,90)
    board.addShip( sf )
    board.addShip( ls )

    sf.setPosition(    0,  0,  0 )  # pointing north
    ls.setPosition(   30,  0,  0 ) # pointing north

    asteroid = Asteroid( 'ast1', create_diamond( (0,0), (7,4) ) )
    board.addObstacle( asteroid )
    asteroid.setPosition( 40, 40, 30 )

    draw_board( board )    
    input("none")


if __name__ == "__main__":
    main()
    test2()
    test2b()
    test2c()
    test3()
    #testPark()
    test4()    
    test4b()
    testBarrel()
    #testParkInterceptor()
    testGame()
    testGame2()
    testSquare()

    testObstacle()

    testPartialBank3()

    testPartialBank1(0)
    testPartialBank1(90)
    testPartialBank1(180)
    testPartialBank1(30)


    testCollision()
    testCollision2()
    testCollision3()
    testCollision4()

#    import pdb; pdb.set_trace()

