class ManeuverDial:
    def __init__( self, maneuver, maneuverColor ):
        self.maneuver = maneuver
        self.maneuverColor = maneuverColor

    def __str__( self ):
        return "Dial: %s (color: %s)" % ( self.maneuver, self.maneuverColor )

class Dial:
    def __init__( self ):
        self.maneuverDials = []
        self.selectedManeuverIndex = None

    def addManeuver(self, maneuver, maneuverColor ):
        self.maneuverDials.append(ManeuverDial(maneuver,maneuverColor))

    def selectManeuver( self, maneuver ):
        for i in range(len(self.maneuverDials)):
            if self.maneuverDials[i].maneuver == maneuver:
                self.selectedManeuverIndex = i
                return True
        print(( "error: This dial does not have this maneuver (%s)" % maneuver))
        return False


    def selectedManeuver( self ):
        return self.maneuverDials[ self.selectedManeuverIndex ]

    def __str__(self):
        return "Dial: %s" % self.selectedManeuver()

