from attack import *
import time

'''
    select the best maneuver/action pair in order to 
    get a good place to attack it.
    - the attacker is supposed to have higher PS than the defender
    - the action is a suggestion; it should be evaluated again after the real movement is revealed
    - points:
       at: at*red dice the attacker will get (ideally, distance 1 and get max dice)
       df: -df*red dice the defender will get (ideally, 0)
       ca: (-3) if it will collide with an asteroid
       oa: (-6) if it will end over an asteroid
       dead: (-12) if it will go outside the board
       
       1) create a tree with all possible attacker movement/action pair (if it's vader?)
       2) expand the tree with all possible defender movement/action pair (same)
       
       although the defender has lower ps and will move first, that won't make any difference
       as dials are selected at the same time.
      
       2b) compute points as above
       3) for each attacker movement/action pair, expand also all next movements for the attacker
       3b) compute collisions and going outside the board points, and aggregate on the parent (the computed move)
       
       4) return the attacked movement (from 1) with the most points
       
'''
class DogFightLowerPs:

    AT=2.0  # red dice for attacker
    DF=6.0  # red dice for defender (against attacker)
    R3=6.0  # in enemy arc at range 3 (1ed autothrusters)
    CA=3.0  # collision with asteroid
    CE=1.0  # collision with enemy ship (TODO end collided)
    OA=50.0 # end overlapping asteroid
    FC=3.0  # ending with focus
    TL=1.0  # ending with target lock (unused)
    EV=1.0  # ending with evade (unused)
    DEAD=100.0 # out of bounds
    
    def __init__(self,  attackerShip, defenderShip ):
        self.attacker = attackerShip
        self.defender = defenderShip


    def getBestMovement2( self, depth=2 ):
        allAttackerMovements = self.getAllMovements( self.attacker )
        allDefenderMovements = self.getAllMovements( self.defender )
        
        bestMovement = None
        start = time.time()

        for i in range(len(allAttackerMovements)):
            at = allAttackerMovements[i]
            print( "[%f] getBestMovement2: %d/%d\n"%(time.time() - start, i,len(allAttackerMovements)) )
            at.score=0
            for df in allDefenderMovements:
                at.score+=self.computeScore2( at, df, depth-1 )

            if bestMovement == None:
                bestMovement = at
            elif bestMovement.score < at.score:
                bestMovement = at

        return bestMovement


    def computeScore2( self, attacker, defender, depth ):
        score = self.computePositionalScore( attacker ) # TODO - self.computePositionalScore( defender )
        if depth==0:
            return score

        attacker.doAction( self.getBestAction( None, attacker, defender ) ) # ideal seria testar todas as ações posicionais
        # TODO PTL
        # TODO check red action

        score += self.computeScore( attacker,  defender )

        allAttackerMovements = self.getAllMovements( attacker )
        allDefenderMovements = self.getAllMovements( defender )

        for i in range(len(allAttackerMovements)):
            at = allAttackerMovements[i]
            for df in allDefenderMovements:
                #print( "\tcomputeScore2: %d/%d (depth=%d)\n"%(i,len(allAttackerMovements),depth) )
                score+=self.computeScore2( at, df, depth-1 )

        return score        


    def getBestMovement(self,depth=1):
        allAttackerMovements = self.getAllMovements( self.attacker )
        allDefenderMovements = self.getAllMovements( self.defender )
        
        bestMovement = None
        
        # to do: modifiers for the defender! for instance,
        # he wont' like to park on asteroid!
        for at in allAttackerMovements:
            at.score = self.computePositionalScore( at )
            if at.isOutOfBoard():
                continue
            allNextAttackerMovements = self.getAllMovements( at )
            for df in allDefenderMovements:
                at.score += self.computeScore( at,  df )
            for nx in allNextAttackerMovements:
                at.score += self.computePositionalScore(at)
            if bestMovement==None or at.score > bestMovement.score:
                bestMovement = at
                
        return bestMovement

    def getBestAction(self, actionDone=None, attacker=None, defender=None):
        if attacker==None:
            attacker = self.attacker
        if defender==None:
            defender = self.defender
        possibleActions = attacker.possibleActions()
        bestAction = None

        for a in possibleActions:
            if a == actionDone:
                continue
            shipClone = attacker.copy()
            shipClone.doAction(a)
            if shipClone.isOutOfBoard():
                continue
            a.score = self.computePositionalScore( shipClone )
            a.score += self.computeScore( shipClone, defender )
            if bestAction == None:
                bestAction = a
            elif bestAction.score < a.score:
                bestAction = a

        return bestAction    


    def computeScore(self,  attacker,  defender ):
        atdf = PrimaryWeaponAttack( attacker,  defender )
        dfat  = PrimaryWeaponAttack( defender,  attacker )
        score = DogFightLowerPs.AT * atdf.redDice()
        score -= DogFightLowerPs.DF * dfat.redDice()
        if dfat.getAttackRange() == 3:
            score += DogFightLowerPs.R3
        return score
        
    def computePositionalScore(self, ship):
        if ship.isOutOfBoard():
            return -DogFightLowerPs.DEAD
        if ship.isOverlappingAsteroid():
            return -DogFightLowerPs.OA
        # TODO bomb detonation 
        score = 0;
        if ship.hasFocus():
            score += DogFightLowerPs.FC
        if ship.hasEvade():
            score += DogFightLowerPs.EV
        #if ship.hasTargetLock():
        #    score += DogFightLowerPs.TL
        return score

    def getAllMovements(self,  ship):
        all = []
        possibleManeuvers = ship.possibleManeuvers()
        for m in possibleManeuvers:
            shipClone = ship.copy()
            shipClone.setDialTo( m )
            shipClone.move()
            if shipClone.isOutOfBoard():
                continue
            all.append( shipClone )
            #for a in shipClone.possibleActions():
            #    shipCloneClone = shipClone.copy()
            #    shipCloneClone.doAction(a)
            #    if shipClone.isOutOfBoard():
            #        continue
            #    all.append( shipCloneClone )
        return all

        
        
if __name__ == "__main__":
    from main import *
    sf = SoontirFelTieInterceptor()
    ls = LukeSkywalkerXwing()
    board = Board(90, 90)
    board.addShip(sf)
    board.addShip(ls)
    sf.setPosition( 0, 0,  0 )
    ls.setPosition( 4, 12,  -180 )
    
    df = DogFightLowerPs( sf,  ls )
    best = df.getBestMovement()
    
    print(best.history)
    print(best.score)
    
    
#    for i in df.getAllMovements(sf):
#        print(i)
 #       print(i.history)
 #       print("\n")
