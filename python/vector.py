#!/usr/bin/python
#Encoding: utf8

import math

class Vector:
    def __init__( self, x, y ):
        self.x = x
        self.y = y

    def __eq__( self, other ):
        return self.dist(other)<0.00001

    def __add__( self, other ):
        return Vector( self.x + other.x, self.y + other.y )

    def __sub__( self, other ):
        return Vector( self.x - other.x, self.y - other.y )

    def __mul__( self, scalar ):
        return Vector( self.x * scalar, self.y * scalar )

    def __str__( self ):
        return "Vector (%f,%f)" % ( self.x, self.y )

    def mod( self ):
        return math.sqrt( self.x**2 + self.y**2 )

    ''' anti-clockwise rotation
        angle => degrees (not rad)
    '''
    def rotate( self, angle ):
        angle = angle / 180.0 * math.pi
        return Vector( self.x * math.cos(angle) - self.y * math.sin(angle),
                        self.x* math.sin(angle) + self.y * math.cos(angle) )

    '''
        clockwise rotation
        angle => degrees (not rad)
    '''
    def rotateClockwise( self, angle ):
        return self.rotate( -angle )

    '''
        distance
    '''
    def dist( self, other ):
        return math.sqrt( (self.x-other.x)**2 + ( self.y-other.y)**2 )

    '''
        clockwise angle
    '''
    def angle( self  ):
        if self.x == 0:
            if self.y > 0:
                return 0
            elif self.y < 0:
                return 180
        elif self.y == 0:
            if self.x > 0:
                return 90
            elif self.x < 0:
                return 270
        elif self.x == self.y == 0:
            return None
        else:
            a = 180 * math.atan( abs( self.y / self.x ) ) / math.pi
            if self.x > 0 and self.y >0: # q1: 0-90
                return 90-a
            elif self.x > 0 and self.y < 0: # q2 90-180
                return 90+a
            elif self.x < 0 and self.y < 0 : #q3 180-270
                return 270-a
            else: #q4 270-360
                return 270+a
'''
    simulate a right bank (speed 1)
'''
def test():
    print("right bank sp1")
    # units: cm and degrees
    p0 = Vector(0,0) # starting position
    w0 = Vector(1,0) # distance between corner and template "button" on base
    w1 = Vector(2,0) # template width
    mi = Vector(0,4) # base height

    # bank 1
    templateInnerRadii = 7.0
    templateAngle = 45.0

    # start calculations
    alpha = (180.0-templateAngle)/2.0
    l = templateInnerRadii * math.sin( math.pi*templateAngle/180.0 )
    ni = Vector( l / math.sin( math.pi*alpha/180.0 ),0 ).rotate( alpha )

    print("NI: %s (mod=%f,alpha=%f)" % (ni,ni.mod(),alpha))

    print( "p0 : %s" % p0)
    print( "+w0: %s" % (p0+w0) )
    print( "+w1: %s" % (p0+w0+w1))
    print( "+ni: %s (ni=%s)" % (p0+w0+w1+ni,ni))
    print( "+mi: %s (mi=%s)" % (p0+w0+w1+ni+mi.rotate(-templateAngle),mi.rotate(-templateAngle)))
    print( "+w1rot: %s (w1=%s)" % (p0+w0+w1+ni+mi.rotate(-templateAngle)+w1.rotate(180-templateAngle),w1.rotate(180-templateAngle)))
    print( "+w0rot: %s (w1=%s)" % (p0+w0+w1+ni+mi.rotate(-templateAngle)+w1.rotate(180-templateAngle)+w0.rotate(180-templateAngle),w0.rotate(180-templateAngle)))


'''
    simulate a left bank (speed 1)
'''
def test2():
    print( "left bank sp1")    # units: cm and degrees
    p0 = Vector(0,0) # starting position
    w0 = Vector(1,0) # distance between corner and template "button" on base
    w1 = Vector(0,0) # template width # mod
    mi = Vector(0,4) # base height

    # bank 1
    templateInnerRadii = 7.0
    templateAngle = 45.0

    # start calculations
    alpha = (180.0-templateAngle)/2.0
    l = templateInnerRadii * math.sin( math.pi*templateAngle/180.0 )
    ni = Vector( l / math.sin( math.pi*alpha/180.0 ),0 ).rotate( 180.0 - alpha ) # mod

   # print("NI: %s (mod=%f,alpha=%f)" % d (ni,ni.mod(),alpha))

    print( "p0 : %s" % p0)
    print("+w0: %s" % (p0+w0))
    print( "+w1: %s" % (p0+w0+w1))
    print( "+ni: %s (ni=%s)" % (p0+w0+w1+ni,ni))
    print( "+mi: %s (mi=%s)" % (p0+w0+w1+ni+mi.rotate(templateAngle),mi.rotate(templateAngle)))
    print( "+w1rot: %s (w1=%s)" % (p0+w0+w1+ni+mi.rotate(templateAngle)+w1.rotate(180-templateAngle),w1.rotate(180-templateAngle))) # mod
    print( "+w0rot: %s (w1=%s)" % (p0+w0+w1+ni+mi.rotate(templateAngle)+w1.rotate(180-templateAngle)+w0.rotate(180-templateAngle),w0.rotate(180-templateAngle))) # mod


'''
    simulate a left turn (speed 1)
'''
def test3():
    print( "left turn sp1"
)    # units: cm and degrees
    p0 = Vector(0,0) # starting position
    w0 = Vector(1,0) # distance between corner and template "button" on base
    w1 = Vector(0,0) # template width #mod
    mi = Vector(0,4) # base height

    # bank 1
    templateInnerRadii = 2.5 # mod 
    templateAngle = 90.0     # mod

    # start calculations
    alpha = (180.0-templateAngle)/2.0
    l = templateInnerRadii * math.sin( math.pi*templateAngle/180.0 )
    ni = Vector( l / math.sin( math.pi*alpha/180.0 ),0 ).rotate( 180.0-alpha ) # mod

    print( "NI: %s (mod=%f,alpha=%f)" % (ni,ni.mod(),alpha) )

    print( "p0 : %s" % p0)
    print( "+w0: %s" % (p0+w0))
    print ("+w1: %s" % (p0+w0+w1))
    print( "+ni: %s (ni=%s)" % (p0+w0+w1+ni,ni))
    print( "+mi: %s (mi=%s)" % (p0+w0+w1+ni+mi.rotate(templateAngle),mi.rotate(templateAngle))) #mod
    print( "+w1rot: %s (w1=%s)" % (p0+w0+w1+ni+mi.rotate(templateAngle)+w1.rotate(180-templateAngle),w1.rotate(180-templateAngle))) # mod
    print( "+w0rot: %s (w1=%s)" % (p0+w0+w1+ni+mi.rotate(templateAngle)+w1.rotate(180-templateAngle)+w0.rotate(180-templateAngle),w0.rotate(180-templateAngle))) # mod

def testangle():
    v = Vector(0, 1)
    print( v.angle() == 0 ) 

    erro = False
    for i in range(359):
        r = round( v.rotateClockwise( i ).angle() )
        if r != i:
            erro = True
            print("Erro em %d (deu %d) "%(i, r))
    if not erro:
        print("angle() consistente com rotateClockwise()!")

if __name__ == "__main__":
    test()
    test2()
    test3()
    testangle()
