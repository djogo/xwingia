#!/usr/bin/python
#Encoding: utf8

import shapely.geometry
import shapely.affinity
from shapely.geometry import Polygon, mapping
import numpy

# TODO - split files
# TODO - set damage
# TODO - create debris

class GameObject:
    def __init__( self, x=None, y=None, angle=None):
        self.positionX = x
        self.positionY = y
        self.angle = angle
        self.board = None

    def setPosition( self, x, y, angle=0.0 ):
        self.positionX = x
        self.positionY = y
        self.angle = angle
#        self.history.addEntry( HistoryManualPositioning( Vector(x,y), self.angle ) )


class Obstacle(GameObject):
    def __init__( self, name, polygon ):
        self.name = name
        self.polygon = polygon
        self.board = None
        GameObject.__init__( self )

    def getShapelyPolygon( self ):

        np = []
        for pi in self.polygon:
            np.append( (pi[0] + self.positionX, pi[1] + self.positionY ) )

        return Polygon( np )

    def setBoard( self, board ):
        self.board = board

    def isAsteroid(self):
        return False

    def isDebris(self):
        return False


class Asteroid(Obstacle):
    def __init__( self, name, polygon ):
        Obstacle.__init__( self, name, polygon ) 

    def isAsteroid(self):
        return True

def create_shapely_ellipse( center, radii ):
    # https://gis.stackexchange.com/questions/243459/drawing-ellipse-with-shapely
    # Let create a circle of radius 1 around center point:
    circ = shapely.geometry.Point(center).buffer(1)

    # Let create the ellipse along x and y:
    ell  = shapely.affinity.scale(circ, radii[0], radii[1] )

    return ell


def create_diamond( center, radii ):

    p0 = ( center[0] - radii[0] , center[1] )
    p1 = ( center[0]            , center[1] - radii[1] )
    p2 = ( center[0] + radii[0] , center[1] )
    p3 = ( center[0]            , center[1] + radii[1] )

    return [p0,p1,p2,p3]

