from maneuver import *

class History:
    def __init__( self ):
        self.entries = []

    def copy( self ):
        h = History()
        h.entries = self.entries[:]
        return h

    def addEntry( self, entry ):
        return self.entries.append( entry )

    def findCloseEntry( self, positionVector, orientation, exceptLast=1 ):
        for i in range(len(self.entries)-exceptLast):
            r = self.entries[i]
            if r.positionVector.dist( positionVector ) < EPSILON and orientation == r.orientation:
                return r
        return False

    def len( self ):
        return len( self.entries )

    def __str__( self ):
        st = []
        for i in self.entries:
            st.append( str(i) )
        return "\n".join(st)


class HistoryBaseEntry:
    def __init__( self, text ):
        self.text = text

    def __str__( self ):
        return "History entry: %s" % ( self.text )

class HistoryManualPositioning(HistoryBaseEntry):
    def __init__( self, positionVector, orientation ):
        self.positionVector = positionVector
        self.orientation = orientation
        self.text = "Manual positioning at position=%s orientation=%f" % ( self.positionVector, self.orientation )


class HistoryDialMovement(HistoryBaseEntry):
    def __init__( self, maneuver, positionVector, orientation, completeness=1.0 ):
        self.maneuver = maneuver
        self.positionVector = positionVector
        self.orientation = orientation
        collision = ""
        if completeness<1.0:
            collision = " with collision (completeness=%.2f)" % completeness
        self.text = "%s position=%s orientation=%f%s" % ( self.maneuver, self.positionVector, self.orientation, collision )

class HistoryAction( HistoryBaseEntry ):
    def __init__( self, action, positionVector, orientation ):
        self.action = action
        self.positionVector = positionVector
        self.orientation = orientation
        self.text = "Action %s position=%s orientation=%f" % ( self.action, self.positionVector, self.orientation )
