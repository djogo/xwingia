from main import *
import turtle

def point( v, scale ):
    turtle.goto( (v.x-45)*scale, (v.y-45)*scale )

def draw_ship( ship, scale ,color="black"):
    turtle.color( color )
    plot_polygon( ship.getShapelyPolygon(), scale )
    plot_polygon( ship.getShapelyMainArc(), scale )

def draw_obstacle( obstacle, scale ,color="black"):
    turtle.color( color )
    plot_polygon( obstacle.getShapelyPolygon(), scale )

def plot_polygon( poly, scale ):
    x,y = poly.exterior.coords.xy
    turtle.up()
    for i in range(len(x)):
        point( Vector( x[i],y[i] ), scale )
        turtle.down()

    point( Vector( x[0],y[0] ), scale )
    turtle.up()



def plot_ship( ship, scale, color="black"):
    p = ship.getSquare()
    turtle.up()
    turtle.color( color )
    for i in [0,1,2,3,0]:
        point( p[i],scale )
        turtle.down()

    point( (p[0]+p[2])*0.5, scale )
    point( p[1], scale )


def draw_board( board ):
    scale=4
    turtle.clear()
    turtle.speed(0)
    turtle.hideturtle()

    turtle.color("black")
    plot_polygon( board.getShapelyPolygon(), scale )

    colors= ["blue","red"]
    for ship in board.getShips():
        draw_ship(ship,scale,colors.pop())

    for obstacle in board.getObstacles():
        draw_obstacle(obstacle,scale)

