from maneuver import *

class ActionBar:
    def __init__( self ):
        self.actionList = [ SkipAction() ]

    def addAction( self, action ):
        self.actionList.append( action )

    def getActions( self ):
        return self.actionList

    def hasAction( self, action ):
        for i in self.actionList:
            if str(i) == str(action): # toscooooo
                return True
        return False

class Action:
    def do( self ):
        pass


class SkipAction(Action):
    def do( self, ship ):
        pass

    def __str__ ( self ):
        return "Skip action"



#TODO avoid performing the same action twice in the same turn
class FocusAction(Action):
    def do( self, ship ):
        ship.addFocus()
        ship.spendAction()

    def __str__ ( self ):
        return "Focus Action"

# TODO detect collision and cancel?
class StraightBoostAction(Action):
    def do( self, ship ):
        x = ManeuverStraight1()
        dv, angleIncrease = x.getDisplacementVector( ship.baseSizeInCm, ship.baseDistanceToTemplate, ship.angle )

        ship.positionX += dv.x
        ship.positionY += dv.y

        if ship.collisionWithAnyShip():
            ship.positionX -= dv.x
            ship.positionY -= dv.y
            return False
        
        ship.spendAction()
        return True

    def __str__ ( self ):
        return "Straight Boost Action"

class SoftLeftBoostAction(Action):
    def do( self, ship ):
        x = ManeuverSoftLeft1()
        dv, angleIncrease = x.getDisplacementVector( ship.baseSizeInCm, ship.baseDistanceToTemplate, ship.angle )

        px = ship.positionX
        py = ship.positionY
        a  = ship.angle

        ship.positionX += dv.x
        ship.positionY += dv.y
        ship.angle = ( ship.angle + angleIncrease + 360 ) % 360

        # TODO asteroids
        if ship.collisionWithAnyShip():
            ship.positionX = px
            ship.positionY = py
            ship.angle = a
            return False        
        
        ship.spendAction()
        return True

    def __str__ ( self ):
        return "Left Boost Action"


class SoftRightBoostAction(Action):
    def do( self, ship ):
        x = ManeuverSoftRight1()
        dv, angleIncrease = x.getDisplacementVector( ship.baseSizeInCm, ship.baseDistanceToTemplate, ship.angle )

        px = ship.positionX
        py = ship.positionY
        a  = ship.angle

        ship.positionX += dv.x
        ship.positionY += dv.y
        ship.angle = ( ship.angle + angleIncrease + 360 ) % 360

        # TODO asteroids
        if ship.collisionWithAnyShip():
            ship.positionX = px
            ship.positionY = py
            ship.angle = a
            return False        
        
        ship.spendAction()
        return True

    def __str__ ( self ):
        return "Right Boost Action"

class BarrelRollAction(Action):
    def setDirectionLeft(self):
        self.barrelDirection = - 90 
        return self
    
    def setDirectionRight(self):
        self.barrelDirection = + 90 
        return self
        
    def setForwardDisplacement(self, dis):
        if  dis<-1 or dis>1:
            raise Exception("Invalid displacement!")
        self.displacement = dis
        
    def __str__(self):
        if  self.barrelDirection == -90:
            side = "left"
        else:
            side = "right"
        return "Barrel Roll to the %s, displacement %d %%" % (side, self.displacement*100)
        
    def do(self, ship):
        x = ManeuverStraight1()
        barrelAngle = ( ship.angle + self.barrelDirection + 360 ) % 360

        px = ship.positionX
        py = ship.positionY
        a  = ship.angle

        # to the side
        ship.positionX += (x.innerRadius + ship.baseSizeInCm) *  math.sin( barrelAngle * math.pi / 180 )
        ship.positionY += (x.innerRadius + ship.baseSizeInCm) * math.cos( barrelAngle * math.pi / 180)
        
        # forward displacement
        #  1 => all the way to the top (ship.x + ship.baseSizeInCm - TEMPLATE_SIZE)
        #  0 => same height (ship.x)
        # -1 => all the way to the bottom (ship.x - ship.baseSizeInCm + TEMPLATE_SIZE)

        dis = self.displacement * ( ship.baseSizeInCm - TEMPLATE_SIZE)
        ship.positionX += (dis) * math.sin( ship.angle * math.pi / 180.0 )
        ship.positionY += (dis) * math.cos( ship.angle * math.pi / 180.0 )

        # TODO asteroids
        if ship.collisionWithAnyShip():
            ship.positionX = px
            ship.positionY = py
            ship.angle = a
            return False        

        ship.spendAction()
        return True

class BarrelRollLeftTopAction(BarrelRollAction):
    def __init__(self):
        BarrelRollAction.__init__(self)
        self.setDirectionLeft()
        self.setForwardDisplacement(1)

class BarrelRollRightTopAction(BarrelRollAction):
    def __init__(self):
        BarrelRollAction.__init__(self)
        self.setDirectionRight()
        self.setForwardDisplacement(1)

class BarrelRollLeftMiddleAction(BarrelRollAction):
    def __init__(self):
        BarrelRollAction.__init__(self)
        self.setDirectionLeft()
        self.setForwardDisplacement(0)

class BarrelRollRightMiddleAction(BarrelRollAction):
    def __init__(self):
        BarrelRollAction.__init__(self)
        self.setDirectionRight()
        self.setForwardDisplacement(0)

class BarrelRollLeftBottomAction(BarrelRollAction):
    def __init__(self):
        BarrelRollAction.__init__(self)
        self.setDirectionLeft()
        self.setForwardDisplacement(-1)

class BarrelRollRightBottomAction(BarrelRollAction):
    def __init__(self):
        BarrelRollAction.__init__(self)
        self.setDirectionRight()
        self.setForwardDisplacement(-1)


