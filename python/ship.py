#!/usr/bin/python3
#Encoding: utf8

from dial import *
from maneuver import *
from action import *
from history import *
from attack import *
import copy
import shapely.wkt
import shapely.geometry

# TODO make it a GameObject 
class Ship:
    def __init__( self, name, ps, pw, ag, hl, sh, dial, actionBar, baseSizeInCm, baseDistanceToTemplate ):
        self.name = name
        self.pilotSkill = ps    # without modifications
        self.primaryWeapon = pw # without modifications
        self.agility = ag       # without modifications
        self.hull    = hl       # without modifications
        self.shield  = sh       # without modifications
        self.dial    = dial     # without modifications
        self.actionBar= actionBar# without modifications
        self.baseSizeInCm = baseSizeInCm
        self.baseDistanceToTemplate = baseDistanceToTemplate

        self.hullDamage = 0
        self.shieldDamage = 0
        self.stressTokens = 0
        self.ionTokens = 0
        self.cloakTokens = 0
        self.focusTokens = 0
        self.evadeTokens = 0

        self.positionX = 0
        self.positionY = 0
        self.angle = 0
        self.board = None

        self.selectedAction = None

        self.actionsPerTurn = 1 # darth vader? PTL?

        self.actionsThisTurn = None # define only after move ship
        self.collisionThisTurn = False

        self.history = History()

    def getName(self):
        return self.name
    '''
        evaluate and attack against defenderShip
        returns an Attack object (primary weapon), or False
    '''
    def attack(self,  defenderShip):
        return PrimaryWeaponAttack( self,  defenderShip )
        
    # TODO distance from squares
    def distanceToShip(self, other):
        vector = self.centerPositionVector() - other.centerPositionVector()
        return vector.mod() - self.baseSizeInCm/2 - other.baseSizeInCm/2

    def collisionWithAnyShip( self ):
        if self.board:
            return self.board.checkCollisionWithOtherShip( self )
        return False
    '''
        despite the name, this function returns
        True whether the self ship and the other
        intersect one with another, not if they bumped
        in this turn.
    '''
    def collisionWithShip( self, other ):
        selfPoly = self.getShapelyPolygon()
        otherPoly = other.getShapelyPolygon()
        return selfPoly.intersects( otherPoly )

    def gotShipInMainArc( self, other ):
        otherPoly = other.getShapelyPolygon()
        return self.getShapelyMainArc().intersects( otherPoly )


    '''
        return the smallest angle to the other ship,
        considering as references:
        - current ship: the base center 
        - other ship: all 4 vertices from the base

        the intended usage is to determine whether another
        ship is in the main attack arc or not.
    '''
    def angleToShip(self, other):

        angles = []
        for vertice in other.getSquare():
            vector = vertice - self.centerPositionVector()
            if vector.angle() is not None: # it happens when vertice = centerPositionVector
                angles.append( ( vector.angle() - self.angle+360 ) % 360 )

        return min(angles)

    '''
        evaluate how many actions are available
        this turn - called by move()
    '''
    def evaluateActions( self ):
        if not self.isStressed():
            self.actionsThisTurn = self.actionsPerTurn
        else:
            self.actionsThisTurn = 0

    def resetTokens( self ):
        self.focusToken = 0
        self.evadeTokens = 0
        self.selectedActions = None
        self.collisionThisTurn = False

    def getPrimaryWeapon(self):
        return self.primaryWeapon
        
    def getPilotSkill(self):
        return self.pilotSkill
        
    def getAgility(self):
        return self.agility
        
    def setPosition( self, x, y, angle=0.0 ):
        self.positionX = x
        self.positionY = y
        self.angle = angle
        self.history.addEntry( HistoryManualPositioning( Vector(x,y), self.angle ) )

    def spendAction( self ):
        self.actionsThisTurn -= 1

    def setBoard( self, board ):
        self.board = board

    def copy( self ):
        x= copy.copy( self )
        x.history = self.history.copy()
        x.dial = copy.copy( self.dial ) 
        x.selectedAction = copy.copy( self.selectedAction )
        return x

    def isOutOfBoard( self ):
        if self.board != None:
            return self.board.isOutOfBoard( self.getShapelyPolygon() )
        return False

    def isOverlappingAsteroid( self ):
        if self.board != None:
            return self.board.isOverlappingAsteroid( self.getShapelyPolygon() )
        return False

    def getSquare( self ):
        
        p0 = self.positionVector()
        p1 = p0 + Vector( 0, self.baseSizeInCm ).rotateClockwise(self.angle+90)
        p2 = p1 + Vector( 0, self.baseSizeInCm ).rotateClockwise(self.angle+180)
        p3 = p0 + Vector( 0, self.baseSizeInCm ).rotateClockwise(self.angle+180)
        
        return [p0, p1, p2, p3]

    '''
        returns a Shapely.geometry.Polygon representing the ship base; it will
        be used to determine collisions and stuff
    '''
    def getShapelyPolygon( self ):
        sq = self.getSquare()
        points = []
        for v in sq:
            points.append( ( v.x, v.y ) )
        return shapely.geometry.Polygon(points)

    '''
        returns a shapely.geometry.Polygon representing the
        main arc 
    '''
    def getShapelyMainArc( self ):
        ma = self.getMainArc()
        points = []
        for v in ma:
            points.append( ( v.x, v.y ) )
        return shapely.geometry.Polygon(points)
        

    '''
        returns a list of Vectors representing the main arc:
        a pentagon, with two segments of line with 30cm (range 3)
        p21 and p29 fix the distance from the ship's front to be
        range 3 too.

       p29___p21
         /   \
     p3 /     \ p2
        \     /
         \   /
          \_/
         p0 p1 <- front of ship's base
        
    '''
    def getMainArc( self ):
        sq  = self.getSquare()
        p0  = sq[0] + Vector( 0.1, 0 ).rotateClockwise( self.angle )
        p1  = sq[1] + Vector( -0.1, 0 ).rotateClockwise( self.angle )
        p2  = p1 + Vector( 0, 30 ).rotateClockwise( self.angle+45 )
        p21 = p1 + Vector( 0, 30 ).rotateClockwise( self.angle )
        p29 = p0 + Vector( 0, 30 ).rotateClockwise( self.angle )
        p3  = p0 + Vector( 0, 30 ).rotateClockwise( self.angle-45 )
        return [p0,p1,p2,p21,p29,p3]

    def beenThereBefore( self ):
        x0 = self.positionX
        y0 = self.positionY
        return self.history.findCloseEntry( Vector(x0,y0), self.angle )

    def __str__( self ):
        return "Name:%s SK=%d X,Y=(%f,%f) angle=%f" % ( self.name, self.pilotSkill, self.positionX, self.positionY, self.angle )

    def isDead(self):
        return self.hull <= self.hullDamage

    def hasShield(self):
        return self.shield <= self.shieldDamage

    def isStressed(self):
        return self.stressTokens > 0

    def isDisabled(self):
        return self.ionTokens > 0

    def isCloaked(self):
        return self.cloakTokens > 0

    def addFocus(self):
        self.focusTokens += 1

    def hasFocus(self):
        return self.focusTokens > 0

    def addEvade(self):
        self.evadeTokens += 1

    def hasEvade(self):
        return self.evadeTokens > 0

    def addStress(self):
        #print( "Increasing 1 stress")
        self.stressTokens+=1

    def removeStress(self):
        #print( "Removing 1 stress")
        self.stressTokens-=1

    def setDialTo( self, maneuver ):
        return self.dial.selectManeuver( maneuver )

    def getSelectedManeuver( self ):
        return self.dial.selectedManeuver()

    def dist( self,v ):
        return Vector( self.positionX, self.positionY ).dist( v )

    '''
        retorna manobras possiveis dado o status da nave
        TODO sair da área de jogo?
        () -> (Boolean)
    '''
    def possibleManeuvers( self ):
        ret = []
        for md in self.dial.maneuverDials:
            if md.maneuverColor.requireNoStress and self.isStressed():
                pass
            else:
                ret.append( md.maneuver )
        return ret


    def possibleActions( self ):
        ret = []
        if not self.isStressed() and self.actionsThisTurn>0 and not self.collisionThisTurn and not self.isOverlappingAsteroid():
            for ac in self.actionBar.getActions():
                # TODO test if action is possible
                ret.append( ac )
        return ret

    '''
        perform all steps of activation phase, except action
        - reveal dial (not applicable)
        - execute maneuver
        -- move ship (TODO: check colision properly)
        -- check stress
        -- clean up (not applicable)
        -- evaluate if there's actions available
    '''
    def move( self ):
        selectedManeuver = self.dial.selectedManeuver()
        if selectedManeuver.maneuverColor.requireNoStress and self.isStressed():
            print(("error: stressed ship may not perform red maneuver"))
            return False

        #print( "performing maneuver %s" % selectedManeuver )

        # this will happen regardless of the movement being finished or not
        if selectedManeuver.maneuverColor.generateStress:
            self.addStress()
        elif selectedManeuver.maneuverColor.clearStress and self.isStressed():
            self.removeStress()
        
        completeness = 1.0

        while completeness > 0.0:

            dv, angleIncrease = selectedManeuver.maneuver.getDisplacementVector( self.baseSizeInCm, self.baseDistanceToTemplate, self.angle, completeness )

            # save it, in case it collides with some other ship
            lastX = self.positionX
            lastY = self.positionY
            angle = self.angle

            # evaluate final position
            self.positionX += dv.x 
            self.positionY += dv.y 
            self.angle = ( self.angle + angleIncrease + 360 ) % 360
            # TODO - angle when collision occurs is not being proporcionated

            # if it has collided, try the movement step by step
            # TODO which ship?
            if self.board and self.board.checkCollisionWithOtherShip( self ):
                self.positionX = lastX
                self.positionY = lastY
                self.angle = angle
                completeness -= 0.001
                self.collisionThisTurn = True
            else:
                break

        # todo mark history indicating collision
        self.history.addEntry( HistoryDialMovement( selectedManeuver, self.positionVector(), self.angle, completeness ) )

        self.evaluateActions()


    def positionVector( self ):
        return Vector( self.positionX, self.positionY )
        
    def centerPositionVector(self):
        return self.positionVector() + Vector( self.baseSizeInCm/2, -self.baseSizeInCm/2 ).rotateClockwise( self.angle)

    def doAction( self, action ):
        if action is None:
            return False
        if self.collisionThisTurn:
            print("Can't perform any action - collision!");
            return False
        res = action.do( self )
        if res:
            self.selectedAction = action
            self.history.addEntry( HistoryAction( action, self.positionVector(), self.angle ) )
        return res

class SmallShip(Ship):
    def __init__( self, name, ps, pw, ag, hl, sh, dial, actionBar ):
        Ship.__init__( self, name, ps, pw, ag, hl, sh, dial, actionBar, 4.00, 1.00 )

class XwingDial(Dial):
    def __init__( self ):
        Dial.__init__(self)
        self.addManeuver( ManeuverStraight1(), GreenManeuver() )
        self.addManeuver( ManeuverSoftLeft1(), GreenManeuver() )
        self.addManeuver( ManeuverSoftRight1(), GreenManeuver() )

        # for testing only
        #self.addManeuver( ManeuverHardLeft1(), GreenManeuver() )
        #self.addManeuver( ManeuverHardRight1(), GreenManeuver() )

        self.addManeuver( ManeuverStraight2(), GreenManeuver() )
        self.addManeuver( ManeuverSoftLeft2(), WhiteManeuver() )
        self.addManeuver( ManeuverSoftRight2(), WhiteManeuver() )
        self.addManeuver( ManeuverHardLeft2(), WhiteManeuver() )
        self.addManeuver( ManeuverHardRight2(), WhiteManeuver() )

        self.addManeuver( ManeuverStraight3(), WhiteManeuver() )
        self.addManeuver( ManeuverSoftLeft3(), WhiteManeuver() )
        self.addManeuver( ManeuverSoftRight3(), WhiteManeuver() )
        self.addManeuver( ManeuverHardLeft3(), WhiteManeuver() )
        self.addManeuver( ManeuverHardRight3(), WhiteManeuver() )

        self.addManeuver( ManeuverStraight4(), WhiteManeuver() )

        self.addManeuver( ManeuverKoiogran4(), RedManeuver() )

class XwingActionBar(ActionBar):
    def __init__(self):
        ActionBar.__init__(self)
        self.addAction( FocusAction() )
        # TODO target lock

class TieInterceptorDial(Dial):
    def __init__( self ):
        Dial.__init__(self)
        self.addManeuver( ManeuverHardLeft1(), WhiteManeuver() )
        self.addManeuver( ManeuverHardRight1(), WhiteManeuver() )

        self.addManeuver( ManeuverStraight2(), GreenManeuver() )
        self.addManeuver( ManeuverSoftLeft2(), GreenManeuver() )
        self.addManeuver( ManeuverSoftRight2(), GreenManeuver() )
        self.addManeuver( ManeuverHardLeft2(), GreenManeuver() )
        self.addManeuver( ManeuverHardRight2(), GreenManeuver() )

        self.addManeuver( ManeuverStraight3(), GreenManeuver() )
        self.addManeuver( ManeuverSoftLeft3(), WhiteManeuver() )
        self.addManeuver( ManeuverSoftRight3(), WhiteManeuver() )
        self.addManeuver( ManeuverHardLeft3(), WhiteManeuver() )
        self.addManeuver( ManeuverHardRight3(), WhiteManeuver() )

        self.addManeuver( ManeuverStraight4(), GreenManeuver() )
        self.addManeuver( ManeuverStraight5(), WhiteManeuver() )

        self.addManeuver( ManeuverKoiogran3(), RedManeuver() )
        self.addManeuver( ManeuverKoiogran5(), RedManeuver() )

class TieInterceptorActionBar(ActionBar):
    def __init__(self):
        ActionBar.__init__(self)
        self.addAction( FocusAction() )
        self.addAction( StraightBoostAction() )
        self.addAction( SoftLeftBoostAction() )
        self.addAction( SoftRightBoostAction() )
        self.addAction( BarrelRollRightBottomAction() )
        self.addAction( BarrelRollRightMiddleAction() )
        self.addAction( BarrelRollRightTopAction() )
        self.addAction( BarrelRollLeftBottomAction() )
        self.addAction( BarrelRollLeftMiddleAction() )
        self.addAction( BarrelRollLeftTopAction() )
        # TODO target lock

class Xwing(SmallShip):
    def __init__( self, name, ps ):
        dial = XwingDial()
        bar = XwingActionBar()
        SmallShip.__init__( self, name, ps, 3, 2, 3, 2, dial, bar ) 


class LukeSkywalkerXwing(Xwing):
    def __init__( self ):
        Xwing.__init__( self, "Luke Skywalker Xwing", 8 )
        # TODO: when defending, you may change 1 eye result to evade

class TieInterceptor(SmallShip):
    def __init__( self, name, ps ):
        dial = TieInterceptorDial()
        bar = TieInterceptorActionBar()
        SmallShip.__init__( self, name, ps, 3, 3, 3, 0, dial, bar ) 


class SoontirFelTieInterceptor(TieInterceptor):
    def __init__( self ):
        TieInterceptor.__init__( self, "Soontir Fel T/I", 9 )
        # TODO: if receive a stress, get a focus


if __name__ == "__main__":
    x = SoontirFelTieInterceptor()
    t = LukeSkywalkerXwing()
    x.setPosition(0,0)
    t.setPosition(1,1)

    print("teste 1: collisionWithShip")
    if x.collisionWithShip(t):
        print( "\tSucesso" )
    else:
        print( "\tFalhou" )
        print(x.getShapelyMainArc())
        print(t.getShapelyPolygon())
        print(x.attack(t))

    t.setPosition( 10,10 )
    print("teste 2: not collisionWithShip")
    if not x.collisionWithShip(t):
        print( "\tSucesso" )
    else:
        print( "\tFalhou" )

    print("teste 3: gotShipInMainArc")
    if x.gotShipInMainArc(t):
        print( "\tSucesso" )
    else:
        print( "\tFalhou" )
        
    print("teste 4: not gotShipInMainArc")
    if not t.gotShipInMainArc(x):
        print( "\tSucesso" )
    else:
        print( "\tFalhou" )
        
    t.setPosition( 40,40,0 )
    print("teste 5: not gotShipInMainArc")
    if not x.gotShipInMainArc(t):
        print( "\tSucesso" )
    else:
        print( "\tFalhou" )
        


