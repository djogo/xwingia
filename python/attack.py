class Attack:
    def __init__( self,  attacker,  defender ):
        self.attacker = attacker
        self.defender = defender
        self.attackDistance = None
        self.inAttackArc = None
        self.attackBonus = 0 # extra dice
        self.defenseBonus = 0 # extra dice
        
    def isInAttackArc(self):
        return self.inAttackArc
        
    def getAttackDistance(self):
        return self.attackDistance
        
    def canAttack(self):
        return False
        
        
    def __str__(self):
        if self.getAttackDistance() == None:
            range = "out of attack range"
        else:
            range = "distance %d" % self.getAttackDistance()
        return "Attack: %s attacking %s %s,  in arc: %s. Can attack: %s. Red dice: %d, green dice: %d." % (
            self.attacker.getName()
            ,  self.defender.getName()
            ,  range
            ,  self.isInAttackArc()
            ,  self.canAttack()
            ,  self.redDice()
            ,  self.greenDice()
            ) 
        
class PrimaryWeaponAttack( Attack ):
    def __init__(self,  attacker,  defender):
        Attack.__init__( self,  attacker,  defender )
        dist = attacker.distanceToShip(defender)

        angle = attacker.angleToShip(defender)
        if angle <= 45 or angle >= 315:
            self.inAttackArc = True
        else:
            self.inAttackArc = False
        
        #TODO use shapely to do all that?
        if dist < 10.0:
            self.attackDistance = 1
            self.attackBonus += 1
        elif dist < 20.0:
            self.attackDistance = 2
        elif dist < 30.0:
            self.attackDistance = 3
            self.defenseBonus += 1
        else:
            self.attackDistance = None # out of range
            
    
    # TODO check for touching bases
    def canAttack(self):
        return self.attacker.gotShipInMainArc( self.defender ) and ( not self.attacker.isOverlappingAsteroid() )

    def getAttackRange(self):
        return self.attackDistance

    def greenDice(self):
        return self.defender.getAgility() + self.defenseBonus

    def redDice(self):
        if self.canAttack():
            return self.attacker.getPrimaryWeapon() + self.attackBonus
        else:
            return 0
