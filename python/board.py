import shapely.wkt
import shapely.geometry

class Board:
    def __init__( self, sizex=90, sizey=90 ):
        self.sizex = sizex
        self.sizey = sizey
        self.ships = []
        self.obstacles = []
        self.polygon = shapely.geometry.Polygon( [ (0,0), (0,sizex), (sizey,sizex), (sizey,0), (0,0) ] )

    def getShapelyPolygon(self):
        return self.polygon

    def addShip( self, ship ):
        self.ships.append(ship)
        ship.setBoard( self )

    def addObstacle( self, obstacle ):
        self.obstacles.append( obstacle )
        obstacle.setBoard( self )

    def isOutOfBoard( self, shapelyPolygon ):
        return not self.polygon.contains( shapelyPolygon )

    def isOverlappingAsteroid( self, shapelyPolygon ):
        for a in self.obstacles:
            # todo in which class this check should be?
            if a.isAsteroid() and a.getShapelyPolygon().intersects( shapelyPolygon ):
                return True

        return False

    '''
        test collision of ship with other ships in
        this board.
        a ship shall not collide with itself
    '''
    def checkCollisionWithOtherShip( self, ship ):
        for s in self.ships:
            if s != ship and s.collisionWithShip( ship ):
                return True
        return False

    def checkOutsideShips( self ):
        outsideShips = []
        for s in self.ships:
            if s.isOutOfBoard():
                outsideShips.append( s )
        return outsideShips

    def getShips( self ):
        return self.ships

    def getObstacles( self ):
        return self.obstacles


