#!/usr/bin/python3

from main import *
from dogFight import *
from consoleDraw import *
import numpy
import turtle
import time

def safeinput(prompt):

    validInput = False

    while not validInput:

        x=input(prompt)

        try:
            return int(x)
        except ValueError:
            print("Entre um valor inteiro!")


def main():
    player   = LukeSkywalkerXwing()
    computer = SoontirFelTieInterceptor()

    board = Board(90,90)
    board.addShip(player)
    board.addShip(computer)

    asteroid = Asteroid( 'ast1', create_diamond( (0,0), (7,4) ) )
    asteroid.setPosition( 40, 40, 30 )
    board.addObstacle( asteroid )

    asteroid = Asteroid( 'ast2', create_diamond( (0,0), (2,3) ) )
    asteroid.setPosition( 60, 50, 30 )
    board.addObstacle( asteroid )

    asteroid = Asteroid( 'ast3', create_diamond( (0,0), (1,3) ) )
    asteroid.setPosition( 70, 70, 00 )
    board.addObstacle( asteroid )

    asteroid = Asteroid( 'ast4', create_diamond( (0,0), (2,2) ) )
    asteroid.setPosition( 20, 20, 45 )
    board.addObstacle( asteroid )

    asteroid = Asteroid( 'ast5', create_diamond( (0,0), (4,1) ) )
    asteroid.setPosition( 20, 70, 210 )
    board.addObstacle( asteroid )

    asteroid = Asteroid( 'ast6', create_diamond( (0,0), (1,1) ) )
    asteroid.setPosition( 40, 30, 30 )
    board.addObstacle( asteroid )

    pos = input("Entre a posição x,y,theta da sua nave (y<10)")
    x,y,theta = pos.split(",")
    player.setPosition( float(x), float(y), float(theta) )

    pos = input("Entre a posição x,y,theta do inimigo (enter para calcular automaticamente)")

    if pos != "":
        x,y,theta = pos.split(",")
        computer.setPosition( float(x), float(y), float(theta) )
    else:
        if float(y) < 45:
            enemytheta = 180
        else:
            enemytheta = 0

        computer.setPosition( float(x)+4,90-float(y), enemytheta ) #down

    while True:
        print("you      " + str(player) )
        print( "Stress: %s" % player.isStressed() )
        print("computer " + str(computer) )
        print( "Stress: %s" % computer.isStressed() )
        draw_board( board )

        print("Computador ataca:")
        print(computer.attack(player))

        print("Jogador ataca:")
        print(player.attack(computer))

        print("Escolhendo manobra do computador...")
        dog_fight = DogFightLowerPs( computer, player )
        computer_maneuver = dog_fight.getBestMovement2()
        computer.setDialTo( computer_maneuver.getSelectedManeuver().maneuver )

        print("Escolha seu dial:")
        for i in range(len(player.possibleManeuvers())):
            print( "%d %s" % ( i, player.possibleManeuvers()[i] ) )
        maneuver_index = int(safeinput("> "))

        player.setDialTo( player.possibleManeuvers()[maneuver_index] )
        print( "Stress: %s" % player.isStressed() )

        print("Movendo sua nave:")
        player.move()
        print( "Stress: %s" % player.isStressed() )
        draw_board( board )
        input("Pressione enter para continuar")

        if len(player.possibleActions()) > 0:
            print( "Ações: %d" % player.actionsThisTurn )
            print( "Escolha sua ação:")
            for i in range(len(player.possibleActions())):
                print( "%d %s" % ( i, player.possibleActions()[i] ) )
            action_index = safeinput("> ")

            player.doAction( player.possibleActions()[action_index] )

        print("Movendo computador:")
        print( computer.getSelectedManeuver() )
        computer.move()
        draw_board( board )
        input("Pressione enter para continuar")

        print("Escolhendo ação:")
        # TODO: compute optimal actions now
        bestAction = dog_fight.getBestAction()
        print( bestAction )
        # TODO: two actions (PTL, Vader)
        if computer.doAction( bestAction ):
            print("\taction was done.")
        else:
            print("\taction not performed.")


        if bestAction.__str__() != "Skip action" and bestAction != None:       # emulating push the limit
            bestAction2 = dog_fight.getBestAction( bestAction )
            print( "Segunda ação:" )
            print( bestAction2 )
            if bestAction2 != None:
                computer.doAction( bestAction2 )
                computer.addStress()


        # TODO if this action is not possible, try another

        if computer.collisionWithShip( player ):
            print("Colisão das naves!")

        if len( board.checkOutsideShips() )>0:
            print("Naves que saíram do tabuleiro:")
            print( board.checkOutsideShips() )

        print("Computador ataca:")
        print(computer.attack(player))

        print("Jogador ataca:")
        print(player.attack(computer))

        computer.resetTokens()
        player.resetTokens()
        input("Pressione enter para continuar")



main()
