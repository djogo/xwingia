import math
from vector import *
from movementTables import *

TEMPLATE_SIZE = 2 # cm
EPSILON = 0.5 # cm

class Maneuver:
    def __init__( self, speed, innerRadius, maneuverType ):
        self.speed = speed
        self.innerRadius = innerRadius
        self.maneuverType = maneuverType
        self.movementTables = MovementTables()
        self.myClassNameString = type(self).__name__

    def __str__( self ):
        return "%s speed %s" % ( self.maneuverType, self.speed )

    def __eq__( self, other ):
        if self.speed != other.speed:
            return False
        return self.maneuverType == other.maneuverType


    '''
        return ship angle increasement due to this movement (including
        rotation such as koiogran

    '''
    def getAngleIncrease( self ):
        return self.maneuverType.getAngleMovement() + self.maneuverType.getAngleAfter()

    '''
        (float, float, float, float) => Vector
        completeness is a number between 0 and 1 representing
        how much of the total movement should be done (to simulate collisions)
    '''
    def getDisplacementVector( self, baseSizeInCm, baseDistanceToTemplate, shipAngle, completeness=1.0 ):
        None

class ManeuverStraight(Maneuver):
    def getDisplacementVector( self, baseSizeInCm, baseDistanceToTemplate, shipAngle, completeness=1.0 ):
        templateLength = ( float(self.innerRadius) + float(baseSizeInCm) ) * completeness
        dx = templateLength * math.sin( shipAngle * math.pi / 180.0 ) 
        dy = templateLength * math.cos( shipAngle * math.pi / 180.0 ) 
        ret = Vector( dx, dy )

        angleIncrease = self.getAngleIncrease( )

        # only does koiogran turn if movement is complete
        if self.maneuverType.isKoiogran() and completeness<1.0:
            angleIncrease = 0

        return ret, angleIncrease

class ManeuverCurve(Maneuver):
    def getDisplacementVector( self, baseSizeInCm, baseDistanceToTemplate, shipAngle, completeness=1.0 ):
        w0 = Vector(baseDistanceToTemplate,0) # distance between corner and template "peg" on base
        w1 = Vector(TEMPLATE_SIZE * completeness ,0) # template width
        mi = Vector(0,baseSizeInCm)  # base height

        if completeness<1.0:
            return self.getDisplacementVectorForPartialMovement(baseSizeInCm, baseDistanceToTemplate, shipAngle, completeness)

        templateAngle = self.maneuverType.getAngleMovement() 
        angleIncrease = self.getAngleIncrease( ) 

        r = self.innerRadius

        # start calculations
        alpha = (180.0-abs(templateAngle))/2.0
        l = r * math.sin( math.pi*abs(templateAngle)/180.0 )
        ni = Vector( l / math.sin( math.pi*alpha/180.0 ),0 )

        if self.maneuverType.isLeft():
            ni = ni.rotate( 180.0 - alpha )
            mi = mi.rotate(abs(templateAngle))
            w1 = Vector(0,0)
            w0w1r = (w0+w1).rotate(180+abs(templateAngle)) #mod
            dv = w0+ni+mi+w0w1r
        else: # right
            ni = ni.rotate( alpha )
            mi = mi.rotate(-templateAngle)
            w0w1r = (w0+w1).rotate(180-templateAngle)
            dv = w0+w1+ni+mi+w0w1r

        dv = dv.rotate(-shipAngle)
        return dv, angleIncrease

    def getDisplacementVectorForPartialMovement( self, baseSizeInCm, baseDistanceToTemplate, shipAngle, completeness ):
    
        # position table
        posXZ = self.movementTables.posXZ[ self.maneuverType.getInclinationType() ][ self.speed ]
        rotY = self.movementTables.rotY[ self.maneuverType.getInclinationType() ][ self.speed ]

        # corner to middle
        ctm = Vector( baseSizeInCm/2, -baseSizeInCm/2 )      
        index = round( completeness * (len(posXZ)-1) )

        if self.maneuverType.isRight():
            dv = Vector( posXZ[index][0]/10, posXZ[index][1]/10 ) # convert to cm
            angleIncrease = rotY[index]

        else:
            dv = Vector( -posXZ[index][0]/10, posXZ[index][1]/10 ) # convert to cm
            angleIncrease = 180.0-rotY[index]

        dv = ctm + dv - ctm.rotate(-angleIncrease)
                
        return dv.rotate(360-shipAngle), angleIncrease


class ManeuverType:
    def __init__( self, name, angleMovement, angleAfter=0.0 ):
        self.name = name
        # angle in which the movement is performed
        self.angleMovement = angleMovement
        # angle of rotation after the movement (eg. koiogran)
        self.angleAfter = angleAfter

    def __eq__( self, other ):
        return self.name == other.name

    def __str__( self ):
        return self.name

    def getAngleMovement( self ):
        return self.angleMovement

    def getAngleAfter( self ):
        return self.angleAfter

    # TODO: horário ou anti-horario?
    def isRight( self ):
        return self.angleMovement>0

    # TODO: horário ou anti-horario?
    def isLeft( self ):
        return self.angleMovement<0

    def isStraight( self ):
        return self.angleMovement==0

    def isBank( self ):
        return abs(self.angleMovement)==45

    def isTurn( self ):
        return abs(self.angleMovement)==90

    def isKoiogran( self ):
        return self.isStraight() and self.angleAfter == 180

    def getInclinationType( self ):
        if self.isStraight():
            return 'straight'
        elif self.isBank():
            return 'bank'
        elif self.isTurn():
            return 'turn'
        else:
            return 'unknown'


class ManeuverTypeTurnLeft(ManeuverType):
    def __init__( self ):
        ManeuverType.__init__(self,"Turn Left (90)",-90.0)

class ManeuverTypeTurnRight(ManeuverType):
    def __init__( self ):
        ManeuverType.__init__(self,"Turn Right (90)",90.0)

class ManeuverTypeBankLeft(ManeuverType):
    def __init__( self ):
        ManeuverType.__init__(self,"Bank Left (45)",-45.0)

class ManeuverTypeBankRight(ManeuverType):
    def __init__( self ):
        ManeuverType.__init__(self,"Bank Right (45)",45.0)

class ManeuverTypeStraight(ManeuverType):
    def __init__( self ):
        ManeuverType.__init__(self,"Straight",0.0)

class ManeuverTypeKoiogran(ManeuverType):
    def __init__( self ):
        ManeuverType.__init__(self,"Koiogran",0.0,180.0)


#
# maneuvers
#
class ManeuverStraight1(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 1, 4.0, ManeuverTypeStraight() )

class ManeuverStraight2(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 2, 8.0, ManeuverTypeStraight() )

class ManeuverStraight3(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 3, 12.0, ManeuverTypeStraight() )

class ManeuverStraight4(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 4, 16.0, ManeuverTypeStraight() )

class ManeuverStraight5(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 5, 20.0, ManeuverTypeStraight() )

class ManeuverSoftLeft1(ManeuverCurve): # bank
    def __init__( self ):
        Maneuver.__init__( self, 1, 7.0, ManeuverTypeBankLeft() )

class ManeuverSoftRight1(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 1, 7.0, ManeuverTypeBankRight() )

class ManeuverHardLeft1(ManeuverCurve): # turn
    def __init__( self ):
        Maneuver.__init__( self, 1, 2.5, ManeuverTypeTurnLeft() )

class ManeuverHardRight1(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 1, 2.5, ManeuverTypeTurnRight() )

class ManeuverSoftLeft2(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 2, 12.0, ManeuverTypeBankLeft() )

class ManeuverSoftRight2(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 2, 12.0, ManeuverTypeBankRight() )

class ManeuverHardLeft2(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 2, 5.3, ManeuverTypeTurnLeft() )

class ManeuverHardRight2(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 2, 5.3, ManeuverTypeTurnRight() )

class ManeuverSoftLeft3(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 3, 17.0, ManeuverTypeBankLeft() )

class ManeuverSoftRight3(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 3, 17.0, ManeuverTypeBankRight() )

class ManeuverHardLeft3(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 3, 8.0, ManeuverTypeTurnLeft() )

class ManeuverHardRight3(ManeuverCurve):
    def __init__( self ):
        Maneuver.__init__( self, 3, 8.0, ManeuverTypeTurnRight() )

class ManeuverKoiogran1(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 4, 4.0, ManeuverTypeKoiogran() )

class ManeuverKoiogran2(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 4, 8.0, ManeuverTypeKoiogran() )

class ManeuverKoiogran3(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 4, 12.0, ManeuverTypeKoiogran() )

class ManeuverKoiogran4(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 4, 16.0, ManeuverTypeKoiogran() )

class ManeuverKoiogran5(ManeuverStraight):
    def __init__( self ):
        Maneuver.__init__( self, 4, 20.0, ManeuverTypeKoiogran() )

class ManeuverColor:
    def __init__(self,name,generateStress,requireNoStress,clearStress):
        self.name = name
        self.generateStress = generateStress
        self.requireNoStress = requireNoStress
        self.clearStress = clearStress

    def __str__( self ):
        return self.name


class GreenManeuver(ManeuverColor ):
    def __init__(self):
        ManeuverColor.__init__(self,"green", False, False, True)

class RedManeuver(ManeuverColor ):
    def __init__(self):
        ManeuverColor.__init__(self,"red", True, True, True)

class WhiteManeuver(ManeuverColor):
    def __init__(self):
        ManeuverColor.__init__(self,"white", False, False, False)
